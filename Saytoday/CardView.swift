//
//  CardView.swift
//  Saytoday
//
//  Created by Dylan Southard on 12/11/20.
//

import SwiftUI

struct CardView: View {
    @ObservedObject var sessionManager:CardSessionManager
    @EnvironmentObject var dm:DataManager
    
    var dummyCardText:String = "Testing"
    
    var body: some View {
        if let test = sessionManager.test, sessionManager.finished {
            ZStack {
                TestResultsView(test:test, collection: sessionManager.collection, course:dm.currentCourse)
                
                if sessionManager.shouldReward {
                    
                    TokenAnimation(amount: sessionManager.collection.reward)
                }
            }
            
            
        } else {
            ZStack {
                
                VStack {
                    Spacer()
                    
                    if let test = sessionManager.test{
                        Text("\(FlexText.QuestionNumberLabel) \(sessionManager.currentInterval + 1)/\(test.cards.count)")
                            .font(Font.QuestionNumber)
                            .foregroundColor(.textPrimary)
                            
                    }
                    Text("\(sessionManager.currentCard?.question ?? "error")")
                        .frame(height:60)
                        .font(Font.CardQuestion)
                        .foregroundColor(.textPrimary).onTapGesture(count: 1, perform: {
                            if Presets.DevMode {
                                sessionManager.answered(.correct)
                            }
                        }).animation(.easeIn)
                    
                    if sessionManager.back {
                        
                        Text("\(sessionManager.currentCard?.answer ?? "error")")
                            .frame( height:40)
                            .font(Font.CardAnswer)
                            .foregroundColor(.textSecondary)
                    }
                    HStack(spacing:30) {
                        SLButton(diameter: sessionManager.back ? 90 : 120, type: .speak, currentState: $sessionManager.speakButtonState, onTap:{
                            
                            sessionManager.speechButtonTapped()
                            
                        })
                        if sessionManager.back {
                            SLButton(diameter: 90, type: .listen, currentState: .constant(sessionManager.listenButtonState), onTap:{
                            })
                        }
                    }
                    
                    if sessionManager.back {
                        
                        Text(sessionManager.resultLabelText)
                            .font(Font.CardAnswer)
                            .foregroundColor(.passText)
                            
                    }

                    
                    Spacer()

                        HStack(spacing:25) {
                            if !sessionManager.back {
                                RoundedRecButton(title: FlexText.ShowButtonLabel, color: .showButtonBkg, height: 40, onTap: {
                                    sessionManager.answered(.pass)
                                })
                            }
                            
                            if sessionManager.back || sessionManager.test == nil {
                                RoundedRecButton(title: sessionManager.back ? "NEXT" : FlexText.SkipButtonLabel, color: sessionManager.back ? .nextButtonBkg(correct: sessionManager.currentAnswer == .correct) : .skipButtonBkg, accessory:UIImage.NextArrow, height: 40, onTap: {
                                        
                                        sessionManager.getNextCard()
                                    
                                    
                                })
                            }
                        }
                    
            }
            
                if sessionManager.back {
                    let condition:ConditionalProperty = sessionManager.currentAnswer == .correct ? .correct : .incorrect
                    ZStack {
                        FeedbackAnimation(condition: condition)
                            .allowsHitTesting(false)
                        if condition == .correct && sessionManager.test == nil {
                            TokenAnimation(amount: 1)
                        }
                    }
                    
                }

            }.padding(.bottom, 30)
        }
        }
        
        
        
}

struct CardView_Previews: PreviewProvider {
    
    
    static var previews: some View {
        let lesson = DataManager().dataModel.courses[0].lessons[0]
        CardView(sessionManager: CardSessionManager(lesson, test:lesson.createTest()))
    }
}



enum AnswerType:String {
    
    case correct
    case incorrect
    case close
    case pass
    case none
    
    var labelFlexText:String {
        switch self {
        case .correct:
            return FlexText.CardCorrectLabel
        case .incorrect:
            return FlexText.CardIncorrectLabel
        case .close:
            return FlexText.CardCloseLabel
        case .pass:
            return FlexText.CardPassLabel
        case .none:
            return ""
        }
    }
    
}

enum SessionType {
    case test
    case study
    case pretest
    case final
    
    var introFlexText:[String] {
        switch self {
        
        case .test:
            return FlexText.TestIntro
        case .study:
            return FlexText.StudyIntro
        case .pretest:
            return FlexText.PretestIntro
        case .final:
            return FlexText.FinalIntro
        }
    }
    
}

enum SessionState {
    case awaitingSpeech
    case speaking
    case processingSpeech
    case viewingHint
    case answered
    
    
    func helpFlexText(forType type:SessionType)->[String] {
        
        switch self {
        
        case .awaitingSpeech:
            
            var _FlexText:[String] = FlexText.AwaitingSpeechHelp
            
            if type == .study {_FlexText.append(contentsOf:FlexText.AwaitingSpeechHelpPractice)}
            
            return _FlexText
            
        case .speaking:
            
            return FlexText.SpeakingHelp
            
        case .processingSpeech:
            
            return FlexText.ProcessingSpeechHelp
            
        case .viewingHint:
            
            return FlexText.ViewingHintHelp
            
        case .answered:
            
            var _FlexText:[String] = FlexText.AnsweredHelp
            
            if type == .study {_FlexText.append(contentsOf: FlexText.AnsweredHelpPractice)}
            
            return _FlexText
            
        }
        
    }
    
}
