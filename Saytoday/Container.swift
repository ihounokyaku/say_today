//
//  SwiftUIView.swift
//  Saytoday
//
//  Created by Dylan Southard on 12/20/20.
//

import SwiftUI

struct Container: View {
    
    var body: some View {
        VStack {
            CourseView()
            BottomBar()
        }
    }
}

struct Container_Previews: PreviewProvider {
    static var previews: some View {
        Container()
    }
}

struct BottomBar:View {
    @EnvironmentObject var dm:DataManager
    var body: some View {
        HStack {
            Text("\(dm.tokens)")
                .foregroundColor(Color(UIColor.navText))
                .padding(.horizontal)
            Spacer()
        }
        .frame(height:BottomFrameHeight)
        .background(Color(UIColor.navMenuBkg))
        
    }
}
