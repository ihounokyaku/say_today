//
//  ContentView.swift
//  Saytoday
//
//  Created by Dylan Southard on 12/7/20.
//

import SwiftUI



struct CourseView: View {
    @EnvironmentObject var dm:DataManager
    @State var currentOpenHeader:Int = -1
    @State var shouldAnimate:Bool = false
    
    var body: some View {
        NavigationView {
            VStack {
                ForEach(dm.dataModel.courses.indices, id:\.self) {index in
                    let course = dm.dataModel.courses[index]
                    
                    CollapsibleTile(headerTitle: course.name, index: index, subTiles: [
                        SubTileData(title: "Study", destination: {
                            AnyView(LessonView(course:course, onPurchased: dm.refreshTokens))
                            
                        }),
                        SubTileData(title: "Pretest", destination:
                                        {
                                            let sessionManager = CardSessionManager(course.pretest)
                                            return AnyView( CardView(sessionManager: sessionManager).onAppear(perform: {
                                                sessionManager.resetTest()
                                            }))
                                        })
                        
                    ], currentOpenHeader:$currentOpenHeader,
                    shouldAnimate: $shouldAnimate, onTappedHeader:{
                        if currentOpenHeader == index {
                            dm.currentCourse = course
                        } else {
                            dm.currentCourse = nil
                        }
                    })
                    
                    
                }
                
                
                Spacer()
                    
                    
                    .navigationBarTitle("Courses", displayMode: .inline)
                    
                
            }.navigationBarColor(backgroundColor: .navMenuBkg, titleColor: .navText, font:Font.NavBarTitle)
            
            
        }.accentColor(.navMenuBtn)
        
        
    }
}

struct CourseView_Previews: PreviewProvider {
    static var previews: some View {
        CourseView()
            .environmentObject(DataManager())
    }
}
