//
//  Presets.swift
//  MaximumEnglish
//
//  Created by Dylan Southard on 11/16/19.
//  Copyright © 2019 Dylan Southard. All rights reserved.
//

import UIKit

class Presets: NSObject {
    
    static let NumTestQuestions:Int = 10
    static let NumFinalTestQuestions:Int = 10
    static let PassThresh:Double = 0.9
    static var updateJSONURL:URL { return DylanJSON }
    static var DevMode = true
    
    static let MaxJSON = URL(string:
    "https://gist.githubusercontent.com/maximzhu/65dfe1609528d0ccb74212b63ec19897/raw/MainData.json")!
    
    static let MaxJSONOld = URL(string:
    "https://gist.githubusercontent.com/maximzhu/58542cb27f43148030f367a436bb1b26/raw/AppData.json")!
    
    static let DylanJSON = URL(string:
    "https://gist.githubusercontent.com/ihounokyaku/f8c65ce162335c52175346e2c59a784c/raw/dataUpdate.json")!
    //just a comment
    //"https://gist.githubusercontent.com/ihounokyaku/f8c65ce162335c52175346e2c59a784c/raw/dataUpdate.json")!
}
