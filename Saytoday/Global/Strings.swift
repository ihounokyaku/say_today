//
//  Strings.swift
//  MaximumEnglish
//
//  Created by Dylan Southard on 10/7/20.
//  Copyright © 2020 Dylan Southard. All rights reserved.
//

import Foundation

enum Language:String, CaseIterable {
    case english = "English"
    case thai = "Thai"
}

class FlexText {
    
    static func LocalText(english:String, thai:String)->String {
        switch UserData.CurrentLanguage {
        case .english:
            return english
        case .thai:
            return thai
        }
    }
    
    //MARK: - =============== Course List ===============
    
    //MARK: - ===  HELP  ===
    static var Welcome:String {LocalText(english: "Welcome to *Say Today!*", thai: "TODO")}
    static var TapCourse:String {LocalText(english: "Tap on a *course* to get started", thai: "TODO")}
    static var TappedCourse:String {LocalText(english: "You can tap on *Pretest* to test your skills or *Study* to begin learning", thai: "TODO")}
    
    //MARK: - ===  LABELS  ===
    static var CoursesTitle:String {LocalText(english:"Courses", thai: "TODO")}
    static var PretestTitle:String {LocalText(english:"Pretest", thai: "TODO")}
    static var StudyTitle:String {LocalText(english:"Study", thai: "TODO")}
    
    //MARK: - ===  ALERTS  ===
    static var CheckingUpdates:String {LocalText(english:"Checking for updates...", thai: "TODO")}
    
    
    
    
    
    //MARK: - =============== Lesson List ===============
    
    //MARK: - ===  HELP  ===
    static var LessonWelcome:String {LocalText(english: "This is the *Lesson* view", thai: "TODO")}
    static var TapLesson:String {LocalText(english:"Choose a *lesson* to get started", thai: "TODO")}
    static var TappedLessonOne:String {LocalText(english: "You can tap *practice* to build your skills", thai: "TODO")}
    static var TappedLessonTwo:String {LocalText(english: "When you are ready, you can take the test", thai: "TODO")}
    static var TappedLessonThree:String {LocalText(english: "Tap *test results* to see your results", thai: "TODO")}
    
    
    //MARK: - ===  LABELS  ===
    static var PracticeTitle:String {LocalText(english: "Practice", thai: "TODO")}
    static var TestTitle:String {LocalText(english: "Test", thai: "TODO")}
    static var TestResultsTitle:String {LocalText(english: "Test Results", thai: "TODO")}
    static var FinalTitle:String {LocalText(english: "Final Test", thai: "TODO")}
    
    //MARK: - ===  ALERTS  ===
    static var LessonLockedTitleInsuffientTokens:String {LocalText(english: "Not enought tokens!", thai: "TODO")}
    static var LessonLockedMessageInsuffientTokens:String {LocalText(english: "You can earn more tokens by reviewing more or by some more at the store", thai: "TODO")}
    
    static var LessonLockedTitle:String {LocalText(english: "Unlock this lesson?", thai: "TODO")}
    static func LessonLockedMessage(cost:Int)->String {LocalText(english: "You can do so for \(cost) tokens", thai: "TODO")}
    static var UnlockButton:String {LocalText(english: "Unlock", thai: "TODO")}
    static var DontUnlockButton:String {LocalText(english: "Not now", thai: "TODO")}
    
    
    //MARK: - =============== Card ===============
    
    //MARK: - ===  HELP  ===
    
    // --- intro text --//
    
    static var TestIntro:[String] {[LocalText(english: "This is the lesson *Test*", thai: "TODO"),
                                        LocalText(english: "Passing will *unlock* the next lesson!", thai: "TODO"),
                                         LocalText(english: "You must get \(Int(Presets.PassThresh * 100))% to pass.", thai: "TODO")] }
    
    static var StudyIntro:[String] {[LocalText(english: "This is the *study* area.", thai: "TODO"),
                                        LocalText(english: "This will allow you to *practice* for the lesson *test*.", thai: "TODO")] }
    
    static var PretestIntro:[String] {[LocalText(english:"This is a *pretest*", thai: "TODO"),
                                        LocalText(english: "Here, you can check your knowledge of the course material", thai: "TODO")] }
    
    static var FinalIntro:[String] {[LocalText(english:"This is the *final test*", thai: "TODO"),
                                        LocalText(english: "It will cover *everything* in this course.", thai: "TODO")] }
    
    //-- Card Actions --//
    
    static var AwaitingSpeechHelp:[String] {[LocalText(english:"The *goal* is to correctly say the word/phrase in English", thai: "TODO"),
                                         LocalText(english: "You can type the *mic* button to speak your answer", thai: "TODO"),
                                         LocalText(english: "If don't know, you can oress *show* to give up", thai: "TODO")] }
    
    static var AwaitingSpeechHelpPractice:[String] {[LocalText(english:"Tap *skip* to move to the next card", thai: "TODO"),
                                         LocalText(english: "Tap *lightbulb* button to get/edit a *hint*", thai: "TODO")] }
    
    static var SpeakingHelp:[String] {[LocalText(english:"Please Speak your answer. When you are finished, press the *mic* button again", thai: "TODO")] }
    
    static var ProcessingSpeechHelp:[String] {[LocalText(english:"Please *wait* while your answer is processed", thai: "TODO")] }
    
    static var AnsweredHelp:[String] {[LocalText(english:"Please *wait* while your answer is processed", thai: "TODO")] }
    
    static var AnsweredHelpPractice:[String] {[LocalText(english:"Tap the *mic* again to practice your pronunciation", thai: "TODO"),
                                      LocalText(english: "Tap the notebook items *explanations* from Max", thai: "TODO")] }
    
    static var ViewingHintHelp:[String] {[LocalText(english:"Here, you can add a custom *hint* to help you in your studies.", thai: "TODO"),
                                      LocalText(english: "Just type your hint and press *save* when you're finished", thai: "TODO")] }
    
    
    //MARK: - ===  LABELS  ===
    // -- ANSWER MESSAGES --//
    static var CardCorrectLabel:String {LocalText(english: "Correct!", thai: "TODO")}
    static var CardIncorrectLabel:String {LocalText(english: "Incorrect!", thai: "TODO")}
    static var CardCloseLabel:String {LocalText(english: "Close!", thai: "TODO")}
    static var CardPassLabel:String {LocalText(english: "Pass", thai: "TODO")}
    static var QuestionNumberLabel:String {LocalText(english: "Question", thai: "TODO")}
    
    static var CorrectMessage:String {LocalText(english: "Well done!", thai: "TODO")}
    static var IncorrectMessage:String {LocalText(english: "You can tap the microphone button again to practice!", thai: "TODO")}
    static var CloseMessage:String {LocalText(english: "You almost got it, listen and try again!", thai: "TODO")}
    
    // -- HINT/NOTES VIEW -- //
    static var HintLabel:String {LocalText(english: "Hint", thai: "TODO")}
    static var NotesLabel:String {LocalText(english: "Explanation", thai: "TODO")}
    static var EditLabel:String {LocalText(english: "Edit", thai: "TODO")}
    static var SaveLabel:String {LocalText(english: "Save", thai: "TODO")}
    static var CancelLabel:String {LocalText(english: "Cancel", thai: "TODO")}
    
    static var NoHintMessage:String {LocalText(english: "Tap \"Edit\" to add a custom hint for this card.", thai: "TODO")}
    static var NoDescriptionMessage:String {LocalText(english: "No explanation available.", thai: "TODO")}
    
    // -- BUTTONS -- //
    
    static var SkipButtonLabel:String {LocalText(english: "SKIP", thai: "TODO")}
    static var ShowButtonLabel:String {LocalText(english:"SHOW", thai: "TODO")}
    //MARK: - ===  ALERTS  ===
    static var SpeechRecErrorTitle:String {LocalText(english: "Sorry, I didn't catch that!", thai: "TODO")}
    static var SpeechRecErrorMessage:String {LocalText(english: "Please make sure your microphone is enabled and that you are connected to the internet.", thai: "TODO")}
    
    
    
    //MARK: - =============== TEST RESULTS ===============
    
    //MARK: - ===  HELP  ===
    static var TestListIntro:[String] {[LocalText(english: "Here, you can view your past test scores for this lesson", thai: "TODO")] }
    static var TestListHelp:[String] {[LocalText(english: "Tap on a *test* to see the details", thai: "TODO")] }
    
    static var TestViewIntroPassed:[String] {[LocalText(english: "Well done! You passed!", thai: "TODO")] }
    static var TestViewIntroFailed:[String] {[LocalText(english: "It looks like this one could use some more work", thai: "TODO"),
                                              LocalText(english: "...but practice makes perfect!", thai: "TODO"),
                                              LocalText(english: "Study the lesson a bit more and try again!", thai: "TODO")] }
   
    static var TestViewHelp:[String] {[LocalText(english: "You can click on each individual *question* for details", thai: "TODO")]}
    
    
    //MARK: - ===  LABELS  ===
    static var ScoreLabel:String {LocalText(english:"Score", thai: "TODO")}
    static var ResultsTitle:String {LocalText(english:"Results", thai: "TODO")}
    static var PassLabel:String {LocalText(english:"PASSED!", thai: "TODO")}
    static var FailLabel:String {LocalText(english:"FAILED!", thai: "TODO")}
    
    static var CourseLabel:String {LocalText(english:"Course", thai: "TODO")}
    static var CollectionLabel:String {LocalText(english:"Lesson", thai: "TODO")}
    static var DateLabel:String {LocalText(english:"Date", thai: "TODO")}
    static var TimeLabel:String {LocalText(english:"Time", thai: "TODO")}
    
    //MARK: - ===  ALERTS  ===
    static var CardNotAvailable:String {LocalText(english:"Card not available", thai: "TODO")}
    
    
    //MARK: - =============== OTHER ===============
    static var PretestPassedMessage:String {LocalText(english:"Well done! You have passed the pretest! You can either move on to the next course, or practice this one a bit more! You can always learn more!", thai: "TODO")}
    
    static var PretestFailedMessage:String {LocalText(english:"Looks like you could use some more practice on this one! Go back to the courses screen and click \"Study\" master this course!", thai: "TODO")}
     
    //MARK: - ===  BUTTONS  ===
    static var RestorePurchases:String {LocalText(english:"Restore Purchases", thai: "TODO")}
    
}

