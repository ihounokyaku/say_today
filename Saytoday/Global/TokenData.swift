//
//  TokenData.swift
//  MaximumEnglish
//
//  Created by Dylan Southard on 11/28/20.
//  Copyright © 2020 Dylan Southard. All rights reserved.
//

import Foundation
import SwiftKeychainWrapper
import SwiftUI

private struct KeychainKey {
    static let encodedData = "encoded_token_data"
    static let tokensRemaining = "tokens_remaining"
    static let tokensSpent = "tokens_spent"
    static let lessonsPurchased = "lessons_purchased"
    static let collectionsPassed = "collections_passed"

}


class KeychainHandler {
    
    static var TokensRemaining:Int {
        get {
            
            return KeychainWrapper.standard.integer(forKey: KeychainKey.tokensRemaining, isSynchronizable: true) ?? 0 }
        set {
           
            KeychainWrapper.standard.set(newValue, forKey: KeychainKey.tokensRemaining, isSynchronizable: true)
            
        }
        
    }
    
    static var LessonsPurchased:[String] {
        if let data = KeychainWrapper.standard.data(forKey: KeychainKey.lessonsPurchased, isSynchronizable: true), let array = try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data) as? [String]{
            return array
        }
        return []
    }
    
    static var CollectionsPassed:[String]{
        if let data = KeychainWrapper.standard.data(forKey: KeychainKey.collectionsPassed, isSynchronizable: true), let array = try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data) as? [String]{
            return array
        }
        return []
    }

    
    private static var _TokensSpent:Int {
        get {return KeychainWrapper.standard.integer(forKey: KeychainKey.tokensSpent, isSynchronizable: true) ?? 0 }
        set { KeychainWrapper.standard.set(newValue, forKey: KeychainKey.tokensSpent, isSynchronizable: true)}
    }
    
    static func PurchaseLesson(withID id:String, forPrice price:Int) {
        do {
            var purchased = LessonsPurchased
            purchased.append(id)
            let data = try NSKeyedArchiver.archivedData(withRootObject: purchased, requiringSecureCoding: true)
            KeychainWrapper.standard.set(data, forKey: KeychainKey.lessonsPurchased, isSynchronizable: true)
            TokensRemaining -= price
            _TokensSpent += price
            
        } catch {
            print(error)
        }
    }
    
    static func setPassedForCollection(withID id:String) {
        do {
            var passed = CollectionsPassed
            passed.append(id)
            let data = try NSKeyedArchiver.archivedData(withRootObject: passed, requiringSecureCoding: true)
            KeychainWrapper.standard.set(data, forKey: KeychainKey.collectionsPassed, isSynchronizable: true)
        
        } catch {
            print(error)
        }
    }
    

}




class TokenDataO : NSObject, NSCoding {
    
    private var _tokensRemaining:Int = 0
    private var _tokensSpent:Int = 0
    private var _lessonsPurchased:[String] = []
    
    static let shared:TokenDataO = {
        return KeychainWrapper.standard.object(forKey: KeychainKey.encodedData) as? TokenDataO ?? TokenDataO()
    }()
    
    var tokensRemaining:Int {
        get { return _tokensRemaining }
        set {
            _tokensRemaining = newValue
            save()
        }
        
    }
    var tokensSpent:Int {return _tokensSpent}
    var lessonsPurchased:[String] {return _lessonsPurchased}
    
    convenience required init?(coder decoder: NSCoder) {
        self.init()
        
        _tokensRemaining = decoder.decodeInteger(forKey: KeychainKey.tokensRemaining)
        _tokensSpent = decoder.decodeInteger(forKey: KeychainKey.tokensSpent)
        _lessonsPurchased = decoder.decodeObject(forKey: KeychainKey.lessonsPurchased) as? [String] ?? []
        
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(_tokensRemaining, forKey: KeychainKey.tokensRemaining)
        coder.encode(_tokensSpent, forKey: KeychainKey.tokensSpent)
        coder.encode(_lessonsPurchased, forKey:KeychainKey.lessonsPurchased)
        
    }
    
    //MARK: - Save
    private func save() {
        do {
            let encodedData = try NSKeyedArchiver.archivedData(withRootObject: self, requiringSecureCoding: true)
            KeychainWrapper.standard.set(encodedData, forKey: KeychainKey.encodedData, isSynchronizable: true)
        } catch let error {
            print(error)
        }
    }
    
    
    
}
