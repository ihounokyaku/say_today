//
//  UserData.swift
//  MaximumEnglish
//
//  Created by Dylan Southard on 11/20/19.
//  Copyright © 2019 Dylan Southard. All rights reserved.
//

import UIKit

private enum FirstTimeKey: String, CaseIterable {
    case course = "first_time"
    case lesson = "first_time_lesson"
    case practice = "first_time_practice"
    case test = "first_time_test"
    case pretest = "first_time_pretest"
    case final = "first_time_final"
    case testList = "test_list"
    case testResults = "test_results"
}



class UserData: NSObject {
    
    static var CurrentLanguage:Language {
        
        get {
            
            guard let lanStr = UserDefaults.standard.value(forKey: "language") as? String, let _language = Language(rawValue: lanStr) else {return .english}
            
            return _language
        }
        
        set { UserDefaults.standard.setValue(newValue.rawValue, forKey: "language")}
        
    }

    static var lastJSONID:String {
        
        get { return UserDefaults.standard.value(forKey: "jsonID") as? String ?? "" }
        
        set { UserDefaults.standard.setValue(newValue, forKey: "jsonID")}
        
    }
    
    static var firstTime:Bool {
        
        get { return UserDefaults.standard.value(forKey: FirstTimeKey.course.rawValue) as? Bool ?? true }
        
        set { UserDefaults.standard.setValue(newValue, forKey: FirstTimeKey.course.rawValue)}
        
    }
    
    static var firstTimeLessons:Bool {
        
        get { return UserDefaults.standard.value(forKey: FirstTimeKey.lesson.rawValue) as? Bool ?? true }
        
        set { UserDefaults.standard.setValue(newValue, forKey: FirstTimeKey.lesson.rawValue)}
        
    }
    
    static var firstTimePractice:Bool {
        
        get { return UserDefaults.standard.value(forKey: FirstTimeKey.practice.rawValue) as? Bool ?? true }
        
        set { UserDefaults.standard.setValue(newValue, forKey: FirstTimeKey.practice.rawValue)}
        
    }
    
    static var firstTimeTest:Bool {
        
        get { return UserDefaults.standard.value(forKey: FirstTimeKey.test.rawValue) as? Bool ?? true }
        
        set { UserDefaults.standard.setValue(newValue, forKey: FirstTimeKey.test.rawValue)}
        
    }
    
    static var firstTimePretest:Bool {
        
        get { return UserDefaults.standard.value(forKey: FirstTimeKey.pretest.rawValue) as? Bool ?? true }
        
        set { UserDefaults.standard.setValue(newValue, forKey: FirstTimeKey.pretest.rawValue)}
        
    }
    
    static var firstTimeFinal:Bool {
        
        get { return UserDefaults.standard.value(forKey: FirstTimeKey.final.rawValue) as? Bool ?? true }
        
        set { UserDefaults.standard.setValue(newValue, forKey: FirstTimeKey.final.rawValue)}
        
    }
    
    static var firstTimeTestList:Bool {
        
        get { return UserDefaults.standard.value(forKey: FirstTimeKey.testList.rawValue) as? Bool ?? true }
        
        set { UserDefaults.standard.setValue(newValue, forKey: FirstTimeKey.testList.rawValue)}
        
    }
    
    static var firstTimeTestResults:Bool {
        
        get { return UserDefaults.standard.value(forKey: FirstTimeKey.testResults.rawValue) as? Bool ?? true }
        
        set { UserDefaults.standard.setValue(newValue, forKey: FirstTimeKey.testResults.rawValue)}
        
    }
    
    static func ResetAllUserData() {

    }
    
    static func ResetFirstTime() {
        
        for key in FirstTimeKey.allCases {
            UserDefaults.standard.setValue(true, forKey: key.rawValue)
        }
    }
    
    
    
}







