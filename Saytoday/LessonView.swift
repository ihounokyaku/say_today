//
//  LessonView.swift
//  Saytoday
//
//  Created by Dylan Southard on 12/10/20.
//

import SwiftUI

struct LessonView: View {
    var course:Course
    var onPurchased:()->Void
    @State var currentOpenHeader:Int = -1
    @State var shouldAnimate:Bool = false
    @State var showAlert = false
    @State var pressedLesson:Lesson?
   
    var body: some View {
        
        VStack(spacing:0) {
            ForEach(course.lessons.indices, id:\.self) {index in
                let lesson = course.lessons[index]
                
                CollapsibleTile(headerTitle: lesson.name, index: index, subTiles: [
                    SubTileData(title: FlexText.StudyTitle, destination: {AnyView(CardView(sessionManager: CardSessionManager(lesson)))}),
                    SubTileData(title: FlexText.TestTitle, destination: {
                        
                        let sessionManager = CardSessionManager(lesson)
                        let cardView = CardView(sessionManager: sessionManager)
                        
                        return AnyView(cardView.onAppear(perform: {
                            sessionManager.resetTest()
                        }))
                        
                    }),
                    SubTileData(title: FlexText.TestResultsTitle, destination: {
                                    AnyView(TestListView(collection: lesson))
                    })
                
                ], currentOpenHeader:$currentOpenHeader, shouldAnimate:$shouldAnimate, locked:lesson.locked, cost:lesson.cost, onTappedLocked: {
                    pressedLesson = lesson
                    showAlert.toggle()
                })
                .onTapGesture(count: /*@START_MENU_TOKEN@*/1/*@END_MENU_TOKEN@*/, perform: {
                    
                    currentOpenHeader = currentOpenHeader == index ? -1 : index
                })
            }
            Spacer()
                .navigationBarTitle(course.name, displayMode: .inline)

            
        }.alert(isPresented: $showAlert, content: {
            Alerts.unlockLesson(cost: pressedLesson!.cost, unlockAction: {
                                    KeychainHandler.PurchaseLesson(withID: pressedLesson!.id, forPrice: pressedLesson!.cost)
                onPurchased()
            })
        })
    }
}

struct LessonView_Previews: PreviewProvider {
    static var previews: some View {
        LessonView(course:DataManager().dataModel.courses[0], onPurchased: {})
    }
}
