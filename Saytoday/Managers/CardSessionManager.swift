//
//  CardManager.swift
//  Saytoday
//
//  Created by Dylan Southard on 12/12/20.
//

import Foundation
import SwiftUI
import Speech


class CardSessionManager: ObservableObject, SpeechRecognizerDelegate {
    
    var collection:Collection
    var test:Test?
  
    lazy var speechRecognizer = {SpeechRecognizer(delegate: self)}()
    
    @Published var currentInterval:Int = -1
    @Published var currentCard:Card? = nil
   
    @Published var currentAnswer:AnswerType = .none {
        didSet {
            withAnimation{
                back = currentAnswer != .none
                resultLabelText = currentAnswer.labelFlexText
            }
        }
    }
    
    @Published var back:Bool = false
    @Published var speakButtonState:SLButtonState = .awaitingInput
    @Published var listenButtonState:SLButtonState = .awaitingInput
    @Published var triggerAnimation:Bool = false
    @Published var resultLabelText:String = ""
    @Published var finished:Bool = false
    @Published var shouldReward = false
    
    init(_ collection: Collection, test: Test? = nil) {
        print("initting session manager")
        self.collection = collection
        self.test = test
       
        self.getNextCard()
    }

    
    var isTest:Bool {return self.test != nil || self.collection.type == .pretest}
    
    func resetTest() {
        self.test = collection.createTest()
        self.finished = false
        self.currentInterval = -1
        self.currentCard = nil
        self.shouldReward = false
        self.getNextCard()
    }
    
    func getNextCard() {
        self.speechRecognizer.cancelAll()
        self.currentAnswer = .none
        self.speakButtonState = .awaitingInput
        if let test = self.test {
            self.currentInterval += 1
             
            if self.currentInterval < test.cards.count {
            
            
            self.currentCard = test.cards[self.currentInterval]
                
            
            } else {
                test.finish()
                if collection.type != .pretest && test.passed && !KeychainHandler.CollectionsPassed.contains(collection.id) {
                    KeychainHandler.TokensRemaining += collection.reward
                 
                    KeychainHandler.setPassedForCollection(withID: collection.id)
                    self.shouldReward = true
    
                }
                self.finished = true
                self.currentCard = nil
            }
            
        } else {
            
            var sortedCards = collection.cards.sorted(by: {$0.userData.interval < $1.userData.interval})
            
            let topInterval:Int = sortedCards.last?.userData.interval ?? 0
            
            let bottomInterval:Int = sortedCards.first?.userData.interval ?? 0
            
            let maxInterval = Int.random(in: bottomInterval...topInterval)
            
            sortedCards = sortedCards.filter({$0.userData.interval <= maxInterval})
            
            if sortedCards.count > 0 {
                let cardIndex = Int.random(in: 0..<sortedCards.count)
                self.currentCard = sortedCards[cardIndex]
                self.currentAnswer = .none
            } else {
                self.currentCard = nil
            }
        
        }
        
        
    }
    
    func answered(_ answerType:AnswerType, record:Bool = true) {
        if self.test == nil && answerType == .correct {
            KeychainHandler.TokensRemaining += 1
            
        }
        self.speakButtonState = .awaitingInput
        let generator = UINotificationFeedbackGenerator()
        generator.notificationOccurred(answerType == .correct ? .success : .error)
        
        self.currentAnswer = answerType
        
        guard let card = self.currentCard, record else {return}
        
        card.userData.timesSeen += 1
        
        switch answerType {
        case .correct:
            card.userData.interval += 1
        case .incorrect, .pass, .close:
            card.userData.timesIncorrect += 1
                       
            if card.userData.interval > 0 { card.userData.interval -= 1 }
        case .none:
            break
        }
        
        if let test = self.test { test.markAnswer(forCard: card, correct: answerType == .correct) }
        
    }
    
    
    
    //MARK: - =============== SPEECH RECOGNITION ===============
    
    
    func speechButtonTapped() {
        
        switch speakButtonState {
        
        case .awaitingInput:
            self.speechRecognizer.getSpeech()
            self.speakButtonState = .active
            print("set state  to \(self.speakButtonState) should set opacity")
        case .active:
            self.speechRecognizer.finishSpeaking()
            self.speakButtonState = .processing
            print("set state to \(self.speakButtonState) should set opacity")
        case .processing:
            break
        }
        
        
    }
    
    func speechResultReturned(result: SFSpeechRecognitionResult) {
        
        guard let card = currentCard else {return}
        for transcription in result.transcriptions {
                        print(transcription.formattedString)
            print("\(transcription.formattedString.lettersOnly())")
                        let segments = transcription.segments
        
                        for segment in segments {
                            print(segment.substring)
                            print(segment.confidence)
                        }
                    }
        
        var answerType:AnswerType = .incorrect
        
        var acceptableAnswers = [card.answer.lettersOnly()]
        acceptableAnswers.append(contentsOf: card.alternates.map{$0.lettersOnly()})
        
        for answer in acceptableAnswers {
            
            if result.bestTranscription.formattedString.lettersOnly() == answer {
                
                answerType = .correct
                break
                
            } else {
                print("the answer \(result.bestTranscription.formattedString.lettersOnly()) is not the correct answer of \(answer)")
                let allTranscriptions = result.transcriptions.map {$0.formattedString.lettersOnly()}
                if allTranscriptions.firstIndex(of: card.answer.lettersOnly()) != nil {

                    answerType = .close
                    
                }
            }
            
        }
        

        self.answered(answerType, record: currentAnswer != .none)
        
        self.speakButtonState = .awaitingInput

    }
    
   
    
    func speechRecognitionError(error: String) {
        //TODO
        self.speakButtonState = .awaitingInput
        
    }
    
    func didBeginRecordingAudio() {
       
        self.speakButtonState = .active
    }
    
    
    
    
    
    
}
