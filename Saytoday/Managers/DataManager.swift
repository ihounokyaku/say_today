//
//  DataPopulator.swift
//  MaximumEnglish
//
//  Created by Dylan Southard on 2019/10/19.
//  Copyright © 2019 Dylan Southard. All rights reserved.
//

import UIKit
import SwiftUI
import Alamofire
import Combine


final class DataManager: ObservableObject {
    @Published var dataModel = JSONModel.default
    @Published var currentCourse:Course? = nil
    @Published var fetchingWebData:Bool = false
    @Published var tokens:Int = KeychainHandler.TokensRemaining
    
    func refreshTokens() { tokens = KeychainHandler.TokensRemaining }
    
    static var DataFolderPath:String = {
        let _path = NSSearchPathForDirectoriesInDomains(
            .applicationSupportDirectory, .userDomainMask, true
        ).first! + "/" + Bundle.main.bundleIdentifier!
        
        // create parent directory iff it doesn’t exist
        try? FileManager.default.createDirectory(
            atPath: _path, withIntermediateDirectories: true, attributes: nil
        )
        
        print(_path)
        return _path
    }()
    
    static var JSONFileURL:URL = {
       
        return URL(fileURLWithPath: DataFolderPath).appendingPathComponent("data.json")
    }()

    
    func executeUpdateQuery() {
        self.fetchingWebData = true
        WebQueryManager(delegate: self).executeDataRequest(atURL: Presets.updateJSONURL)
        
    }
    
    func PopulateInitialData() {
        
        
        DBManager.CreateTables()
        
    }
    
}

extension DataManager:WebQueryDelegate {
    
    func dataQueryComplete(manager: WebQueryManager, results: Data?, error: String?) {
        
        guard let data = results else {
            
            self.fetchingWebData = false
            print("error fetching data \(String(describing: error))")
            return
        }
        
        do {
           let _dataModel = try JSONDecoder().decode(JSONModel.self, from: data)
            if _dataModel.id != dataModel.id {
                
                dataModel = _dataModel
                
                self.fetchingWebData = false
                
                if FileManager.default.fileExists(atPath: DataManager.JSONFileURL.path) {
                    
                        try FileManager.default.removeItem(atPath: DataManager.JSONFileURL.path)
                        
                    
                }
                
                try data.write(to: DataManager.JSONFileURL)
                
            } else {
                
                self.fetchingWebData = false
                
            }
            
            
        } catch {
            print("Error parsing data \(error)")
            self.fetchingWebData = false
        }

    }
    
}

