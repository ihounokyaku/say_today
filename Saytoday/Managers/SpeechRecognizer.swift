//
//  SpeechRecognizer.swift
//  MaximumEnglish
//
//  Created by Dylan Southard on 11/10/19.
//  Copyright © 2019 Dylan Southard. All rights reserved.
//

import UIKit
import Speech

protocol SpeechRecognizerDelegate {
    
    func speechResultReturned(result:SFSpeechRecognitionResult)
    func speechRecognitionError(error:String)
    func didBeginRecordingAudio()
    
}

class SpeechRecognizer: NSObject {
    
    let audioEngine = AVAudioEngine()
    let speechRecognizer:SFSpeechRecognizer? = SFSpeechRecognizer(locale: Locale.init(identifier: "en-US"))
    let request = SFSpeechAudioBufferRecognitionRequest()
    var recognitionTask:SFSpeechRecognitionTask?
    var delegate:SpeechRecognizerDelegate?
    var speaking = false
    var totalTimer:Timer?
    var silenceTimer:Timer?
    
    
    init(delegate:SpeechRecognizerDelegate) {
        
        super.init()
        
        self.delegate = delegate
        
    }
    
    func getSpeech() {
        AVAudioSession.setRoute(forCategory: AVAudioSession.Category.record)
                       
        let node = self.audioEngine.inputNode
        let recordingFormat = node.outputFormat(forBus: 0)
        
        node.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { buffer, _ in
            self.request.append(buffer)
        }
        
        audioEngine.prepare()
        do {
            try audioEngine.start()
            self.speaking = true
            self.totalTimer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(self.finishSpeaking), userInfo: nil, repeats: false)
            self.delegate?.didBeginRecordingAudio()
        } catch {
            print("couldn't start audio engine")
            self.delegate?.speechRecognitionError(error: error.localizedDescription)
            return
        }
    }
    
  
    
    @objc func finishSpeaking() {
       
        self.speaking = false
        guard let rec = SFSpeechRecognizer(), rec.isAvailable else {
            self.cancelAll()
            self.delegate?.speechRecognitionError(error: "Recognizer not available")
            return
        }
        
        self.invalidateTimer()
        
        self.cancelAll()
        
        self.recognitionTask = speechRecognizer?.recognitionTask(with:self.request, resultHandler:  { result, error in
            
            if let result = result {
                if result.isFinal {
                    
                    self.delegate?.speechResultReturned(result: result)
                    self.recognitionTask?.finish()
                    self.recognitionTask = nil
                }
                
            } else {
                
                self.recognitionTask?.finish()
                self.recognitionTask = nil
                
                self.delegate?.speechRecognitionError(error: error?.localizedDescription ?? "No speech results")
                
            }
            
        })
    }
    
    func invalidateTimer() {
        self.totalTimer?.invalidate()
        self.totalTimer = nil
        self.silenceTimer?.invalidate()
        self.silenceTimer = nil
    }
    
    func cancelAll() {
        self.invalidateTimer()
        self.recognitionTask = nil
        self.audioEngine.stop()
        self.audioEngine.inputNode.removeTap(onBus: 0)
        self.request.endAudio()
    }
    
    @objc func stoppedSpeakingForOneSecond(){
        print("stoppedSpeakingForOneSecond")
    }
}



extension AVAudioSession {
    static func setRoute(forCategory category:Category) {
        let audioSession = AVAudioSession.sharedInstance()
             do {

                try! audioSession.setCategory(category)
//                try audioSession.setMode(AVAudioSession.Mode.spokenAudio)
                try audioSession.setActive(true)

                            let currentRoute = AVAudioSession.sharedInstance().currentRoute

                                for description in currentRoute.outputs {
                                    print("port type is \(description.portType)")
                                    if description.portType == AVAudioSession.Port.headphones || description.portType == AVAudioSession.Port.bluetoothHFP  || description.portType == AVAudioSession.Port.bluetoothLE  || description.portType == AVAudioSession.Port.bluetoothA2DP {
                                        try audioSession.overrideOutputAudioPort(AVAudioSession.PortOverride.none)
                                        
                                        return

                                    } else {
                                       
                                        try audioSession.overrideOutputAudioPort(AVAudioSession.PortOverride.speaker)
                                    }
                            }

        } catch {
              print("audioSession properties weren't set because of an error.")
        }
    }
}



