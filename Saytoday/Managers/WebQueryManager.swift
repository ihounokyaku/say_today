//
//  WebQuearyManager.swift
//  MaximumEnglish
//
//  Created by Dylan Southard on 11/20/19.
//  Copyright © 2019 Dylan Southard. All rights reserved.
//

import UIKit
import Alamofire

protocol WebQueryDelegate {
    
    func dataQueryComplete(manager:WebQueryManager, results:Data?, error:String?)
    
}

class WebQueryManager: NSObject {
    
    static var CurrentRequest:DataRequest?
    
    var delegate:WebQueryDelegate?
    
    init(delegate:WebQueryDelegate) { self.delegate = delegate }
    
    
    func executeDataRequest(atURL:URL) {
        
        
        //-- Cancel any current requests --//
        if let req = WebQueryManager.CurrentRequest {
            
            
            
            req.cancel() }
        
        //-- setup current request --//
        WebQueryManager.CurrentRequest = AF.request(Presets.updateJSONURL)
        
        //-- execute and get response --//
        WebQueryManager.CurrentRequest?.responseData(completionHandler: { (response) in
           
            WebQueryManager.CurrentRequest = nil
            switch response.result {
            case .success(_):
                
                self.delegate?.dataQueryComplete(manager: self, results: response.data, error: nil)
            case .failure(_):
                self.delegate?.dataQueryComplete(manager: self, results: nil, error: "Could not download updates!")
            }
            
            
        })
    }
    

}
