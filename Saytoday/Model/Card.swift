//
//  CardModel.swift
//  MaximumEnglishDataCreator
//
//  Created by Dylan Southard on 12/3/20.
//  Copyright © 2020 Dylan Southard. All rights reserved.
//

import Foundation
import SQLite


class Card:Codable {
    
    //MARK: - =============== CARD PROPERTIES ===============
    let id:String
    let question:String
    let answer:String
    let questionAudio:String?
    let answerAudio:String?
    let notes:String?
    
    private let _alternates:[String]?
    var alternates:[String]{ return _alternates ?? [] }
    
    
    
    private let _includedInFinal:Bool?
    var includedInFinal:Bool { return _includedInFinal ?? true }
        
    
    
    private let _displayAnswer:String?
    var displayAnswer:String {
        var _text = self._displayAnswer?.withValue ?? self.answer
        
        if let _number = Int(_text) {
            let numberFormatter = NumberFormatter()
            numberFormatter.numberStyle = .decimal
            _text = numberFormatter.string(from: NSNumber(value:_number)) ?? _text
        }
        
        return _text
    }
        
    
    enum CodingKeys:String, CodingKey {
        case id
        case question
        case answer
        case notes
        case _alternates = "alternate_answers"
        case _includedInFinal = "included_in_final"
        case _displayAnswer = "display_answer"
        case questionAudio = "question_audio"
        case answerAudio = "answer_audio"
    }
    
    //MARK: - =============== USER PROPERTIES ===============
    
    
    lazy var userData:CardUserData = {
        
        if let data = DBManager.FetchObject(ofType: .cardData, withID: self.id) as? CardUserData {
            return data
        } else {
            let data = CardUserData(id: self.id)
            DBManager.Insert(object: data)
            return data
        }
    }()
    
    
    
}


class CardUserData: SQLiteObject {
    
    //MARK: - ===  PRIVATE VARS  ===
    private var _hint:String?
    private var _interval:Int
    private var _timesSeen:Int
    private var _timesIncorrect:Int
    
    
    //MARK: - ===  USERACCESSABLE VARS  ===
    var interval:Int {
        
        get { return self._interval }
        
        set {
            self._interval = newValue
            self.update([ObjectProperties.Interval <- newValue])
        }
    }
    
    var timesSeen:Int {
        
        get { return self._timesSeen }
        
        set {
            self._timesSeen = newValue
            self.update([ObjectProperties.TimesSeen <- newValue])
        }
        
    }
    
    var timesIncorrect:Int {
        
        get { return self._timesIncorrect }
        
        set {
            self._timesIncorrect = newValue
            self.update([ObjectProperties.TimesIncorrect <- newValue])
        }
        
    }
    
    var hint:String? {
        
        get { return self._hint }
        
        set {
            self._hint = newValue
            self.update([ObjectProperties.Hint <- newValue ?? ""])
            
        }
    }
    
    
    //MARK: - =============== INIT ===============

    
    init(id:String, hint:String? = nil, interval:Int = 0, timesSeen:Int = 0, timesIncorrect:Int = 0) {

        self._hint = hint
        self._interval = interval
        self._timesSeen = timesSeen
        self._timesIncorrect = timesIncorrect
        
        super.init(id: id, type: .cardData)
    }
    
    convenience init?(fromRow row:Row) {
        do {
            self.init(id: try row.get(ObjectProperties.Id),
                      hint: try row.get(ObjectProperties.Hint).withValue,
                      interval: try row.get(ObjectProperties.Interval),
                      timesSeen: try row.get(ObjectProperties.TimesSeen),
                      timesIncorrect: try row.get(ObjectProperties.TimesIncorrect)
            )
        } catch {
            print(error)
            return nil
        }
    }
    
    //MARK: - =============== INSERT ===============
    override var insertExpression:[Setter] {
        return [
            ObjectProperties.Id <- self.id,
            ObjectProperties.Hint <- self.hint ?? "",
            ObjectProperties.Interval <- self.interval,
            ObjectProperties.TimesSeen <- self.timesSeen,
            ObjectProperties.TimesIncorrect <- self.timesIncorrect,
        ]
    }
}

