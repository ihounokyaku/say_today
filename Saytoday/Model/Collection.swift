//
//  Collection.swift
//  MaximumEnglish
//
//  Created by Dylan Southard on 12/5/20.
//  Copyright © 2020 Dylan Southard. All rights reserved.
//

import Foundation

enum CollectionType:String, CaseIterable {
    
    case custom = "Custom"
    case lesson = "Lesson"
    case final = "Final Test"
    case pretest = "Pretest"
    
}


protocol Collection {
    
    var type:CollectionType { get }
    var name:String { get }
    var id:String { get }
    var cards:[Card] { get }
    var tests:[Test] { get }
    var locked:Bool { get }
    var reward:Int { get }
    var numberTestQuestions:Int { get }
    func createTest()-> Test
}

extension Collection {
    
    var numberTestQuestions:Int { return Presets.NumTestQuestions }
    
    var locked:Bool { return false }
    
    func createTest()-> Test { return Test(cards: Array(self.cards.shuffled().prefix(self.numberTestQuestions)), lessonID: self.id, collectionType:self.type) }
    
    var tests:[Test] {
        var _tests = [Test]()
        
        for result in DBManager.FetchObjects(ofType: .test, withStringProperty: ObjectProperties.CollectionID, value: self.id) {
            
            if let test = result as? Test {
                _tests.append(test)
            }
        }
        
        return _tests
        
    }
}
