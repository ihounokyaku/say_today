//
//  CardModel.swift
//  MaximumEnglishDataCreator
//
//  Created by Dylan Southard on 12/3/20.
//  Copyright © 2020 Dylan Southard. All rights reserved.
//

import Foundation





class Course: Codable {
    
    //MARK: - =============== CODABLE VARS ===============
    let id:String
    let name:String
    let lessons:[Lesson]
    let pretest:Pretest
    let video:String?
    let notes:String?
    let image:String?
    var _reward:Int?
        var reward:Int{
            get { return _reward ?? 100}
            set { _reward = newValue }
        }

    
    enum CodingKeys:String, CodingKey {
        case id
        case name
        case lessons
        case pretest
        case notes
        case image
        case video
        case _reward = "reward"
    }
    
    lazy var finalTest:FinalTest? = { return FinalTest(course:self) }()
    
    //MARK: - =============== COMPUTED VARS ===============
    lazy var allCards:[Card] = {
        var cards = [Card]()
        
        for lesson in self.lessons { cards += lesson.cards }
        
        return cards
    }()
    
    
}
