//
//  SQLiteDataManager.swift
//  MaximumEnglish
//
//  Created by Dylan Southard on 7/8/20.
//  Copyright © 2020 Dylan Southard. All rights reserved.
//

import Foundation
import SQLite




class DBManager: NSObject {
    
    

    //MARK: - =============== DATABASE ===============
    
    

    static var db:Connection = {
        
        let connection = try! Connection("\(DataManager.DataFolderPath)/db.sqlite3")
        
        try! connection.execute("PRAGMA foreign_keys = ON;")
        
        return connection
        
    }()

    
    //MARK: - =============== CREATE ===============
    
    static func CreateTables() {
        for type in ObjectType.allCases {
            
            let properties = type.properties
            
            let table = Table(type.rawValue)
            
            let create = table.create { (t) in
                
                t.column(ObjectProperties.Id, primaryKey: true)
                
                for expression in properties.stringProperties {t.column(expression)}
                
                for expression in properties.intProperties {t.column(expression)}
                
                for key in properties.linkedProperties {t.column(key.expression) }
                
                for key in properties.linkedProperties {

                    t.foreignKey(key.expression, references: key.references, ObjectProperties.Id, delete: key.onDelete)
                    
                }
            }
            do {
                try db.run(create)
            } catch {
                print(error)
            }
        }
    }
    
    static func Insert(object:SQLiteObject, presentError:Bool = false) {
        do {
            
            try self.db.run(object.insert)
            
        } catch {
            
//            if presentError { AlertManager.PresentErrorAlert(withTitle: "Error!", message: "Could not add object to database!") }
            print(error)
        }
    }

    
    //MARK: - =============== READ ===============
    
    
    static func FetchObjects(ofType type:ObjectType, withStringProperty property:Expression<String>, value:String)->[SQLiteObject] {
        
        return FetchObjects(fromTable: type.table.filter(property == value), ofType: type)
        
    }
    
    static func FetchObjects(fromTable table:Table, ofType type:ObjectType)->[SQLiteObject] {
        
        
        do {
            var results = [SQLiteObject]()
            let rows = try db.prepare(table)
            
            for row in rows {
                if let obj = type.newObject(fromRow: row) { results.append(obj) }
            }
            
            return results
        } catch {
            
            print(error)
            
            return []
            
        }
        
    }
    
    static func FetchObject(ofType type:ObjectType, withID id:String)->SQLiteObject? {
        
        return FetchObjects(ofType: type, withStringProperty: ObjectProperties.Id, value: id).first
        
    }
    
    //MARK: - =============== UPDATE ===============
    
    static func Update(object:SQLiteObject, commands:[Setter]) {
        
            let dbObject = object.table.filter(ObjectProperties.Id == object.id)
            let update = dbObject.update(commands)
            do {
                try db.run(update)
            } catch {
                print(error)
            }
        
    }
    
    //MARK: - =============== DESTROY ===============
    
    static func delete(object:SQLiteObject) {
        
        let dbObject = object.table.filter(ObjectProperties.Id == object.id)
       
        let delete = dbObject.delete()
        do {
            try db.run(delete)
        } catch {
            print("could not delete \(object.id)")
            print(error)
            print(error.localizedDescription)
        }
        
    }
    
}
