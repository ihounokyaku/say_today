//
//  SQLiteObject.swift
//  MaximumEnglish
//
//  Created by Dylan Southard on 7/8/20.
//  Copyright © 2020 Dylan Southard. All rights reserved.
//

import UIKit
import SQLite

enum ObjectType:String, CaseIterable{
    case test = "tests"
    case cardData = "user_card_data"
    case testCard = "test_cards"
    
    var properties:PropertySet {
        
        switch self {
        case .cardData:
            return ObjectProperties.CardData
        case .test:
            return ObjectProperties.Test
        case .testCard:
            return ObjectProperties.TestCard
            
        }
    }
    
    var table:Table {return Table(self.rawValue)}
    
    func newObject(fromRow row:Row)->SQLiteObject? {
        switch self {
        case .cardData:
            return Test(fromRow: row)
//            CardUserData(fromRow: row)
        case .test:
            return Test(fromRow: row)
        case .testCard:
            return TestCard(fromRow: row)
        }
    }
    
}

class ObjectProperties {
    
    //MARK: - =============== PROPERTIES ===============
    
    //MARK: - ===  PROPERTIES  ===

    static let Id = Expression<String>("id")
    static let Question = Expression<String>("question")
    static let Answer = Expression<String>("answer")
    static let Hint = Expression<String>("hint")
    static let CardID = Expression<String>("card_id")
    static let CollectionID = Expression<String>("collection_id")
    static let CollectionType = Expression<String>("collection_type")
    
    static let CardLevel = Expression<Int>("card_level")
    static let Interval = Expression<Int>("interval")
    static let TimesSeen = Expression<Int>("times_seen")
    static let TimesIncorrect = Expression<Int>("times_incorrect")
    static let Correct = Expression<Int>("correct")
    static let Number = Expression<Int>("number")
    static let Score = Expression<Int>("score")
    static let DateStarted = Expression<Int>("date_started")
    static let DateFinished = Expression<Int>("date_finished")
    static let Passed = Expression<Int>("passed")
    
    
    
    //MARK: - ===  LINKED PROPERTIES  ===
        
    static let TestID = ForeignKey(keyName: "test_id", referenceType: .test, onDelete: .cascade)
    
    
    
    //MARK: - =============== OBJECT PROPERTY SETS ===============
    
    static let CardData = PropertySet(stringProperties: [Hint], intProperties: [Interval, TimesSeen, TimesIncorrect], linkedProperties: [])

    static let TestCard = PropertySet(stringProperties: [CardID, Question, Answer], intProperties: [Number, Correct], linkedProperties: [TestID])
    
    static let Test = PropertySet(stringProperties: [CollectionID, CollectionType], intProperties: [Score, DateStarted, DateFinished, Passed], linkedProperties: [])
}

class ForeignKey {
    
    var expression:Expression<String>
    var references:Table
    var foreignExpression:Expression<String> {return ObjectProperties.Id}
    var onDelete:TableBuilder.Dependency
    
    init(keyName:String, referenceType:ObjectType, onDelete:TableBuilder.Dependency) {
        self.expression = Expression<String>(keyName)
        self.references = Table(referenceType.rawValue)
        self.onDelete = onDelete
    }
    
}

struct PropertySet {
    var stringProperties:[Expression<String>]
    var intProperties:[Expression<Int>]
    var linkedProperties:[ForeignKey]
}


class SQLiteObject: NSObject {
    
    let id:String
    let type:ObjectType
   
    
    var table:Table {return Table(type.rawValue)}
    
    
    //MARK: - ===  DB COMMANDS  ===
    
    var insert:Insert {return self.table.insert(self.insertExpression)}
    var insertExpression:[Setter] {return []}
    
    init(id:String?, type:ObjectType) {
        
        self.id = id ?? UUID().uuidString
        self.type = type
        super.init()
    }
    
    
    func update(_ setters:[Setter]) { DBManager.Update(object:self, commands:setters) }
}






