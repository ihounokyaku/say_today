//
//  JSONModel.swift
//  MaximumEnglishDataCreator
//
//  Created by Dylan Southard on 12/3/20.
//  Copyright © 2020 Dylan Southard. All rights reserved.
//

import Foundation


class JSONModel: Codable {
    
    var courses:[Course]
    var id:String
    var url:URL?
    
    lazy var allCards:[Card] = {
        var cards = [Card]()
        for course in courses {
            cards.append(contentsOf:course.allCards)
        }
        return cards
    }()
    
    enum CodingKeys:String, CodingKey {
        case id
        case courses
    }
    
    static let `default`:JSONModel = {
        
        if let data = try? Data(contentsOf:DataManager.JSONFileURL), let jsonModel = try? JSONDecoder().decode(JSONModel.self, from: data) {
        return jsonModel
    } else {
        
        let url = Bundle.main.url(forResource: "InitialData", withExtension: "json")!
        
        let data = try! Data(contentsOf: url)
        
        return try! JSONDecoder().decode(JSONModel.self, from: data)
        
    }}()
    
}


