//
//  FinalExam.swift
//  MaximumEnglish
//
//  Created by Dylan Southard on 4/25/20.
//  Copyright © 2020 Dylan Southard. All rights reserved.
//

import Foundation


class FinalTest:Collection {
    
    
//MARK: - =============== COMPUTED VARS ===============
    
    private let _id:String
    private let _course:Course
    
    var reward:Int {return _course.reward}
    var type: CollectionType {return .final}
    var name:String {return "Final" }
    var id:String {return _id }
    var course:Course {return _course}
    var cards:[Card] { return _course.allCards.filter({$0.includedInFinal}) }
    
    var numberTestQuestions:Int {return Presets.NumFinalTestQuestions}
    

        //MARK: - =============== Functions ===============
    init(course:Course) {
        self._id = UUID().uuidString
        self._course = course
        
    }
    
}
