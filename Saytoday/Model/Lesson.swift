//
//  CardModel.swift
//  MaximumEnglishDataCreator
//
//  Created by Dylan Southard on 12/3/20.
//  Copyright © 2020 Dylan Southard. All rights reserved.
//

import Foundation

class Lesson: Codable, Collection {
    

    //MARK: - =============== CODABLE VARS ===============
    
    var id:String
    var name:String
    var cards:[Card]
    
    private var _cost:Int?
    var cost:Int{ get { return _cost ?? 0 } }
    
    private var _reward:Int?
    var reward:Int{ return _reward ?? 100 }
    
    private var notes:String?

    
    var image:String?
    var video:String?
    
    enum CodingKeys:String, CodingKey {
        case id
        case name
        case _reward = "reward"
        case cards
        case _cost = "cost"
        case notes
        case image
        case video
    }
    
    //MARK: - =============== COMPUTED VARS ===============
    var type: CollectionType {return .lesson}
    var locked:Bool {return !(self.cost == 0 || KeychainHandler.LessonsPurchased.contains(self.id))}

    
}
