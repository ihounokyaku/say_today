//
//  CardModel.swift
//  MaximumEnglishDataCreator
//
//  Created by Dylan Southard on 12/3/20.
//  Copyright © 2020 Dylan Southard. All rights reserved.
//

import Foundation

class Pretest: Codable, Collection {
    
    //MARK: - =============== MODEL VARS ===============
    
    var id:String
    var cards:[Card]
    var reward: Int {return 0}
    
    private var _notes:String?
    var notes:String {return _notes ?? "" }
        
    enum CodingKeys:String, CodingKey {
        case id
        case cards
        case _notes = "notes"
    }
    
    //MARK: - =============== COMPUTED VARS ===============
    
    var name: String { return "Pretest" }
    
    var type: CollectionType { return .pretest }
    
}
