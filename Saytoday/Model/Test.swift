//
//  Test.swift
//  MaximumEnglish
//
//  Created by Dylan Southard on 11/2/19.
//  Copyright © 2019 Dylan Southard. All rights reserved.
//

import Foundation
import SQLite


class Test:SQLiteObject {
    
    //MARK: - ================ STORED VARIABLES ================
    
    private var _score:Int
    
    private var _dateStarted:Date
    
    private var _dateFinished:Date?
    
    private var _passed:Bool
    
    private var _cards = [Card]()
    
    private var _collectionID:String
    
    let collectionType:CollectionType
    
    var score:Int {return self._score}
    
    var collectionID:String {return self._collectionID}
    
    var dateStarted:Date {return self._dateStarted}
    
    var dateFinished:Date? {
        get {return self._dateFinished}
        set {_dateFinished = newValue }
        
    }
    
    var passed:Bool {
        get {return self._passed}
        set {self._passed = newValue}
        
    }
    
    var cards:[Card] {return _cards}
    
    
    //MARK: - ================= LINKED OBJECTS =================
    
    
    lazy var answeredCards:[TestCard] = {
        
        var _cards = [TestCard]()
        
        for card in DBManager.FetchObjects(ofType: .testCard, withStringProperty: ObjectProperties.TestID.expression, value: self.id) {
            
            if let realCard = card as? TestCard {
                _cards.append(realCard)
            }
            
        }
        return _cards.sorted(by: {$0.number < $1.number})
        
    }()
    
    //MARK: - =============== COMPUTED VARS ===============
    
    var passThresh:Double {return Presets.PassThresh}
    
    var scoreString:String { return "\(self._score)/\(self.cards.count)" }
    
    var timeUsed:String {
        
        guard let date = self._dateFinished else {return ""}
        
        let interval = date.timeIntervalSince(self._dateStarted)
        
        return interval.toString()
        
    }
    
    //MARK: - ================== INITIALIZERS ==================
    
    init(id:String?,  lessonID:String, dateStarted:Date, dateFinished:Date?,  passed:Bool, score:Int, collectionType:CollectionType) {
        self._dateStarted = dateStarted
        self._dateFinished = dateFinished
        self._score = score
        self._passed = passed
        self._collectionID = lessonID
        self.collectionType = collectionType
        super.init(id: id, type:ObjectType.test)
    }
    
    convenience init(cards:[Card], lessonID:String, collectionType:CollectionType) {
        
        self.init(id:nil, lessonID:lessonID, dateStarted:Date(), dateFinished:nil, passed:false, score:0, collectionType:collectionType)
        
        self._cards = cards
        
    }
    
    convenience init?(fromRow row:Row) {
        
        do {
            self.init(id:try row.get(ObjectProperties.Id),
                      lessonID:try row.get(ObjectProperties.CollectionID),
                      dateStarted:Date.From(int: try row.get(ObjectProperties.DateStarted)),
                      dateFinished:Date.From(int: try row.get(ObjectProperties.DateFinished)),
                      passed:try row.get(ObjectProperties.Passed).toBool,
                      score:try row.get(ObjectProperties.Score),
                      collectionType: CollectionType(rawValue: try row.get(ObjectProperties.CollectionType)) ??  CollectionType.lesson
            )
        } catch {
            print(error)
            return nil
        }
        
    }
    
    
    
    
    //MARK: - =================== FUNCTIONS ====================
    
    func markAnswer(forCard card:Card, correct:Bool) {
        let answeredCard = TestCard(id: nil, cardID: card.id, testID: self.id, correct: correct, number: answeredCards.count + 1, question:card.question, answer:card.answer)
        self.answeredCards.append(answeredCard)
        
        if correct { self._score += 1 }
        
        card.userData.timesSeen += 1
        if !correct { card.userData.timesIncorrect += 1 }
    }
    
    func finish() {
        
        self._passed = Double(self.score) / Double(self.cards.count) >= self.passThresh
        self._dateFinished = Date()
        
        if self.collectionType != .pretest {
            DBManager.Insert(object: self)
        
            for card in self.answeredCards {
               
                DBManager.Insert(object: card)
            }
        }
    }
    
    
    //MARK: - =============== DB COMMANDS ===============
    override var insertExpression:[Setter] {
        return [
            ObjectProperties.Id <- self.id,
            ObjectProperties.Score <- self.score,
            ObjectProperties.DateStarted <- self.dateStarted.toInt,
            ObjectProperties.DateFinished <- self.dateFinished!.toInt,
            ObjectProperties.Passed <- self.passed.toInt,
            ObjectProperties.CollectionID <- self.collectionID,
            ObjectProperties.CollectionType <- self.collectionType.rawValue
        ]
    }
    
    
    
    
}
