//
//  TestCard.swift
//  MaximumEnglish
//
//  Created by Dylan Southard on 7/9/20.
//  Copyright © 2020 Dylan Southard. All rights reserved.
//

import UIKit
import SQLite

class TestCard: SQLiteObject {
    
    var testID:String
    var cardID:String
    var question:String
    var answer:String
    var correct:Bool
    var number:Int
    
    var test:Test? { return DBManager.FetchObject(ofType: .test, withID: self.testID) as? Test }
    
    lazy var originalCard:Card? = {return DataManager().dataModel.allCards.first(where: {$0.id == self.cardID} )}()
    
    init(id:String?, cardID:String, testID:String, correct:Bool, number:Int, question:String, answer:String) {
        
        self.testID = testID
        self.cardID = cardID
        self.correct = correct
        self.number = number
        self.question = question
        self.answer = answer
        
        super.init(id: id, type: .testCard)
    }
    
    convenience init?(fromRow row:Row){
        
        do {
            self.init(id:try row.get(ObjectProperties.Id),
            cardID:try row.get(ObjectProperties.CardID),
            testID:try row.get(ObjectProperties.TestID.expression),
            correct:try row.get(ObjectProperties.Correct).toBool,
            number:try row.get(ObjectProperties.Number),
            question: try row.get(ObjectProperties.Question),
            answer: try row.get(ObjectProperties.Answer)
            )
        } catch {
            print(error)
            return nil
        }
    }
    
    
    //MARK: - =============== DB COMMANDS ===============
    override var insertExpression:[Setter] {
        return [
            ObjectProperties.Id <- self.id,
            ObjectProperties.Number <- self.number,
            ObjectProperties.Correct <- self.correct == false ? 0 : 1,
            ObjectProperties.TestID.expression <- self.testID,
            ObjectProperties.CardID <- self.cardID,
            ObjectProperties.Question <- self.question,
            ObjectProperties.Answer <- self.answer
        ]
        
    }

}
