//
//  Colors.swift
//  Saytoday
//
//  Created by Dylan Southard on 12/25/20.
//

import Foundation
import SwiftUI


extension Color {
    
    //MARK: - =============== MAIN ===============
    
    static var cPrimary:Color = Color("primary")
    
    static var cSecondary:Color = Color("secondary")
    
    static var cSecondaryLight:Color = Color("secondary_light")
    
    static var cWhite:Color = Color("white")
    
    static var cBlack:Color = Color("c_black")
    
    static var cRed:Color = Color("red")
    
    static var cGreen:Color = Color("green")
    
    static var cYellow:Color = Color("yellow")
    

    //MARK: - ===  TEXT  ===
    static var textPrimary:Color {cBlack}
    
    static var textDisabled:Color {textPrimary.opacity(0.7)}
    
    static var textSecondary:Color {cSecondary}
    
    
    //MARK: - ===  NAV  ===
    
    static var navMenuBtn:Color {cWhite}
    
    //MARK: - ===  TABLE  ===
    static var expandButton:Color {cBlack}
    
    static var tableEven:Color {cWhite}
    
    static var tableOdd:Color{cSecondaryLight.opacity(0.4)}
    
    static func tableBkg(_ index:Int)->Color {return index % 2 == 0 ? tableEven : tableOdd}
    
    //MARK: - ===  PASS/FAIL  ===
    
    static var passText:Color {cGreen }
       
    static var failText:Color { cRed }
    
    static func passFailText(_ pass:Bool)->Color { pass ? .passText : .failText}
    
    //MARK: - ===  CARD  ===
//    static var CardLeftTab:UIColor{return Secondary(alpha: 0.9)}
//
//    static var CardLeftTabOutline:UIColor { return PrimaryLight() }
//
//    static var CardLeftTabImage:UIColor {return PrimaryLight()}
    
    static var speakBtnBkg:Color {cGreen.opacity(0.2)}
    
    static var speakBtnOutline:Color { cGreen }
    
    static var speakBtnImage:Color { speakBtnOutline }
    
    static var speakBtnAnimation:Color { cGreen }
    
    static var speakBtnProcessingAnimation:Color { cYellow }
    
    static func nextButtonBkg(correct:Bool) -> Color {passFailText(correct)}
    
    static var skipButtonBkg:Color {cYellow}
    
    static var nextButtonTxt:Color {cWhite}
    
    static var showButtonBkg:Color {cRed}
    
    static var cardListBkg:Color {secondary.opacity(0.3)}
    
    static var practiceText:Color {cGreen}
    
}

extension UIColor {
    
    static var navMenuBkg:UIColor {return UIColor(named:"primary")! }
    
    static var navText:UIColor {return UIColor(named:"c_black")!}
    
}
