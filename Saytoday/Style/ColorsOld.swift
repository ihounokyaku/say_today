////
////  Colors.swift
////  FilmSwipe
////
////  Created by Dylan Southard on 2018/09/27.
////  Copyright © 2018 Dylan Southard. All rights reserved.
////
//
//import Foundation
//import UIKit
//import SwiftUI
//extension UIColor {
//
//
//    
//    convenience init(hexString: String) {
//        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
//        var int = UInt32()
//        Scanner(string: hex).scanHexInt32(&int)
//        let a, r, g, b: UInt32
//        switch hex.count {
//        case 3: // RGB (12-bit)
//            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
//        case 6: // RGB (24-bit)
//            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
//        case 8: // ARGB (32-bit)
//            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
//        default:
//            (a, r, g, b) = (255, 0, 0, 0)
//        }
//        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
//    }
//
//    static func HexWithAlpha(hex:String, alpha:CGFloat)-> UIColor{
//        let color = UIColor(hexString: hex)
//        return color.withAlphaComponent(alpha)
//    }
//
//
//    //MARK: - =============== MAIN PALLETE ===============
//    static func Primary(alpha:CGFloat = 1)-> UIColor{
//        return HexWithAlpha(hex:"#4169e1", alpha:alpha)
//    }
//
//    static func PrimaryLight(alpha:CGFloat = 1)-> UIColor{
//        return HexWithAlpha(hex:"#4169e1", alpha:alpha)
//    }
//
//    static func PrimaryShade(alpha:CGFloat = 1)-> UIColor{
//        return HexWithAlpha(hex:"#e5e5f2", alpha:alpha)
//    }
//
//    static func Secondary(alpha:CGFloat = 1)-> UIColor{
//        return HexWithAlpha(hex:"#efef08", alpha:alpha)
//    }
//
//    static func BlackPrimary(alpha:CGFloat = 1)-> UIColor {
//        return HexWithAlpha(hex:"#535353", alpha:alpha)
//    }
//
//    static func LightGreyPrimary(alpha:CGFloat = 1)-> UIColor {
//        return HexWithAlpha(hex:"#a0a0a0", alpha:alpha)
//    }
//
//    static func WhitePrimary(alpha:CGFloat = 1)-> UIColor {
//        return HexWithAlpha(hex:"#ffffff", alpha:alpha)
//    }
//
//    static func Pass(alpha:CGFloat = 1)-> UIColor {
//        return HexWithAlpha(hex:"#22ac38", alpha:alpha)
//    }
//
//    static func Fail(alpha:CGFloat = 1)-> UIColor {
//        return HexWithAlpha(hex:"#c00a07", alpha:alpha)
//    }
//
//    //MARK: - =============== COLOR BY USE ===============
//
//
//
////    //MARK: - ===  CARD INFO  ===
////
////    static var cardInfoViewBkg:UIColor {return Primary()}
////
////    static var cardInfoViewImg:UIColor {return Secondary()}
////
////    static var cardInfoCategoryText:UIColor {return Secondary()}
////
////    static var cardInfoItemText:UIColor {return WhitePrimary()}
////
////    static var CardInfoHeaderText:UIColor {return WhitePrimary()}
////
////    static var CardInfoTextBoxText:UIColor{return WhitePrimary()}
////
////    static var CardInfoTextBoxBkg:UIColor {return UIColor.clear }
////
////    static var CardInfoDivider:UIColor{return UIColor.Secondary()}
//
//
//    //MARK: - =============== TEST RESULTS ===============
//
//
//}
//
//
//
