//
//  Fonts.swift
//  FilmSwipe
//
//  Created by Dylan Southard on 2018/09/27.
//  Copyright © 2018 Dylan Southard. All rights reserved.
//

import Foundation
import UIKit
import SwiftUI

extension Font {
    
    func fontSizeByDevice(defaultSize:CGFloat, iPhone5:CGFloat, ipad:CGFloat, ipadPro:CGFloat? = nil)-> CGFloat {
        if UIDevice.current.screenType == .iPhones_5_5s_5c_SE {
            return iPhone5
        } else if UIDevice.current.screenType == .ipad {
            if let pro = ipadPro {
                if UIDevice.isiPadPro {return pro}
            }
            
            return ipad
        }
        return defaultSize
    }
    
    static func PrimaryRegular(size:CGFloat)->Font{
        return Font.custom( "PTSans-Regular", size: size)
    }
    static func PrimaryBold(size:CGFloat)->Font{
        return Font.custom( "PTSans-Bold", size: size)
    }
    
    static func PrimaryItalic(size:CGFloat)->Font{
        return Font.custom("PTSans-Italic", size: size)
    }
   
    
    static var AlertTitleFont:Font {return Font.PrimaryBold(size:18)}
    
    static var AlertMessageFont:Font {return Font.PrimaryRegular(size:16)}
    
    static var TableRowTitle:Font {return Font.PrimaryRegular(size: 20)}
    
    static var NavBarTitle:UIFont{return UIFont(name: "PTSans-Regular", size: 18) ?? UIFont.systemFont(ofSize: 18)}
    
    static var RestoreButton:Font{return Font.PrimaryRegular(size: 12)}
    
    
    
    static var HelpMain:Font{return Font.PrimaryRegular(size: 16)}
    
    static var HelpEmphasis:Font{return Font.PrimaryItalic(size: 18)}
    
    static var HelpNavButton:Font{return Font.PrimaryRegular(size: 14)}
    
    
    
    static var CardQuestion:Font{return Font.PrimaryRegular(size: 40)}
    
    static var CardAnswer:Font{return Font.PrimaryRegular(size: 28)}
    
    static var HintTitle:Font{return Font.PrimaryRegular(size: 28)}
    
    static var HintDescription:Font{return Font.PrimaryRegular(size: 17)}
    
    static var NextButton:Font{return Font.PrimaryRegular(size: 22)}
    
    static var CardInfoTopView:Font{return Font.PrimaryRegular(size: 15)}
    
    static var CardInfoCategory:Font{return Font.PrimaryRegular(size: 18)}
    
    static var CardInfoHeader:Font{return Font.PrimaryRegular(size: 25)}
    
    static var CardInfoItem:Font{return Font.PrimaryRegular(size: 18)}
    
    static var CardInfoTextBox:Font{return Font.PrimaryRegular(size: 14)}
    
    static var QuestionNumber:Font{return Font.PrimaryRegular(size: 22)}
    
    
    static var TestList:Font{return Font.PrimaryRegular(size: 15)}
    
    static var TestViewCellAccessory:Font{return Font.PrimaryRegular(size: 13)}
    
    static var TestResultValue:Font{return Font.PrimaryRegular(size: 20)}
    
    static var TestResultCategory:Font{return Font.PrimaryBold(size: 20)}
    
    static var TestResultHeader:Font{return Font.PrimaryRegular(size: 40)}
    
    static var PracticeLabel:Font{return Font.PrimaryRegular(size: 16)}
    
    static var PretestResultMessage:Font{return Font.PrimaryRegular(size: 16)}
    
    static var FeedbackToken:Font{return Font.PrimaryBold(size: 60)}
    
    static var Cost:Font{return Font.PrimaryRegular(size: 12)}
    
}


