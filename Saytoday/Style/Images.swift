//
//  Images.swift
//  MaximumEnglish
//
//  Created by Dylan Southard on 1/12/20.
//  Copyright © 2020 Dylan Southard. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
    
    static var Lock:UIImage { return SafeImage(named: "lock") }
    
    static var HintImage:UIImage {return SafeImage(named:"hint")}
    
    static var DescriptionImage:UIImage {return SafeImage(named:"note")}
    
    static var NextArrow:UIImage {return SafeImage(named:"chevron_right")}
    
    static var CardInfoTop:UIImage {return SafeImage(named:"chevron_up")}
    
    static var CardInfoBottom:UIImage {return SafeImage(named:"pencil")}
    
    static var RestorePurchaces:UIImage{return SafeImage(named: "restore_purchases").withRenderingMode(.alwaysTemplate)}
    
    static var HelpIcon:UIImage{return SafeImage(named: "help").withRenderingMode(.alwaysTemplate)}
    
    static var FeedbackCorrect:UIImage {return SafeImage(named:"check").withRenderingMode(.alwaysTemplate)}
    
    static var FeedbackIncorrect:UIImage {return SafeImage(named:"x").withRenderingMode(.alwaysTemplate)}
    
    static func SafeImage(named name:String)->UIImage {return UIImage(named:name) ?? UIImage()}
    
    static func SLButton(for type:SLButtonType)->UIImage? {
        var imageName = ""

        switch type {
        case .speak:
            imageName = "speak_answer"

        case .listen:
            imageName = "listen"

        }

        return UIImage(named: imageName)

    }
    
    
    
}
