//
//  TestListView.swift
//  
//
//  Created by Dylan Southard on 12/18/20.
//

import SwiftUI

struct TestListView: View {
    var collection:Collection
    
    var dummyTests:[Test] = {
        var _tests = [Test]()
        for i in 0...5 {
            let test = Test(cards: [], lessonID: "123", collectionType: .lesson)
            test.dateFinished = Date()
            test.passed = i % 2 == 0
            _tests.append(test)
        }
        return _tests
    }()
    var body: some View {
        VStack(spacing:0) {
            ForEach(collection.tests.indices, id:\.self) {index in
                let test = collection.tests[index]
                
                NavigationLink(destination:TestResultsView(test: test, collection: collection)) {
                    HStack {
                        Text((test.dateFinished ?? Date()).toString(format: .yearMonthDay))
                            .font(Font.TestList)
                        Spacer()
                        Text(FlexText.ScoreLabel + ": \(test.score)/\(test.answeredCards.count)")
                            
                            .font(Font.TestList)
                            
                        Spacer()
                        ZStack {
                            let recHeight = TableRowHeight * 0.5
                            Rectangle()
                                .fill(Color.passFailText(test.passed).opacity(0.8))
                                .cornerRadius(recHeight / 2)
                                .frame(width:TableRowHeight * 1.2,height:recHeight)
                            Text(test.passed ? FlexText.PassLabel : FlexText.FailLabel)
                                .foregroundColor(.cWhite)
                                .font(Font.TestViewCellAccessory)
                        }
                        
                        
                    }
                    .padding(.horizontal)
                    .frame(height:TableRowHeight)
                    .background(Color.tableBkg(index))
                    
                }
                .foregroundColor(.primary)

            }
            Spacer()
        }
    }
}

struct TestListView_Previews: PreviewProvider {
    static var previews: some View {
        TestListView(collection:DataManager().dataModel.courses[0].lessons[0])
    }
}
