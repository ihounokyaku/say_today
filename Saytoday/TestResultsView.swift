//
//  TestResultsView.swift
//  Saytoday
//
//  Created by Dylan Southard on 12/18/20.
//

import SwiftUI

struct TestResultsView: View {
    
    var test:Test
    var collection:Collection
    var course:Course?
    var prev = false
    func detailValue(for detail:TestResultDetail)->String {
        switch detail {
        
        case .course:
            return course?.name ?? ""
        case .collection:
            return collection.name
        case .date:
            return test.dateStarted.toString(format: .yearMonthDayHourMin)
        case .time:
            return test.timeUsed
        }
    }
    
    var cards:[TestCard] = {
        
        var _cards = [TestCard]()
        for i in 1...20 {
            _cards.append(TestCard(id: "1234\(i)", cardID: "123", testID: "err", correct: true, number: i, question: "question", answer: "anser"))
        }
        return _cards
    }()

    var body: some View {
        VStack(spacing:10) {
            VStack {
                Group {
                    Text("\(test.score)/\(test.answeredCards.count)")
                    Text(test.passed ? FlexText.PassLabel : FlexText.FailLabel)
                }
                .font(Font.TestResultHeader)
                .foregroundColor(.passFailText(test.passed))
            }
            
            VStack(alignment:.leading, spacing:5) {
                ForEach(TestResultDetail.allCases, id:\.self) {detail in
                    TestResultLabel(detail: detail, value: detailValue(for: detail))
                }
            }
            
            ScrollView {
                VStack(spacing:0) {
                    ForEach(test.answeredCards.indices, id:\.self) { index in
                        
                        let card = test.answeredCards[index]
                        TestCardTile(card:card)
                    }
                }
                
            }
                
        }.padding(.top, 40)
    }
}

struct TestResultsView_Previews: PreviewProvider {
    static var previews: some View {
        let course = DataManager().dataModel.courses[0]
        TestResultsView(test:Test(cards: [], lessonID: "id", collectionType: .lesson), collection: course.lessons[0], course: course);
    }
}

struct TestResultLabel:View {
    var detail:TestResultDetail
    var value:String
    var body: some View {
        HStack(alignment:.firstTextBaseline, spacing:15) {
            Text(detail.labelText)
                .font(Font.TestResultCategory)
                .foregroundColor(.textPrimary)
            
            Text(value)
                .font(Font.TestResultValue)
                .foregroundColor(.textPrimary)
            Spacer()
            
        }.padding(.horizontal)
        
    }
    
}

struct TestCardTile:View {
   
    var card:TestCard
    var body: some View {
        HStack {
            Group {
                Text("\(card.number).")
                Text(card.question)
            }
            .font(Font.TableRowTitle)
            .foregroundColor(.textPrimary)
            Spacer()
            ZStack {
                Circle()
                    .foregroundColor(.passFailText(card.correct))
                    .frame(width:TableRowHeight / 2, height:TableRowHeight / 2)
                Text(card.correct ? "✔︎" : "✖︎")
                    .foregroundColor(Color(UIColor.lightText))
            }
            
        }
        .frame(height:TableRowHeight)
        .padding(.horizontal)
        .background(Color.tableBkg(card.number))
    }
}

enum TestResultDetail:String, CaseIterable {
    case course = "Course"
    case collection = "Lesson"
    case date = "Date"
    case time = "Time"
    
    var labelText:String {
        switch self {
        
        case .course:
            return FlexText.CourseLabel
        case .collection:
            return FlexText.CollectionLabel
        case .date:
            return FlexText.DateLabel
        case .time:
            return FlexText.TimeLabel
        }
    }
    
    
}
