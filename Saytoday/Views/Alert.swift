//
//  File.swift
//  Saytoday
//
//  Created by Dylan Southard on 12/16/20.
//

import Foundation
import SwiftUI

class Alerts {
    
    static func unlockLesson(cost:Int,unlockAction: @escaping ()->Void)->Alert {
        if cost > KeychainHandler.TokensRemaining {
            return Alert(title: Text(FlexText.LessonLockedTitleInsuffientTokens), message: Text(FlexText.LessonLockedMessageInsuffientTokens))
        } else {
            print("cost is \(cost)")
            let primaryButton = Alert.Button.default(Text(FlexText.UnlockButton)) {
                unlockAction()
            }
            let secondaryButton = Alert.Button.default(Text(FlexText.DontUnlockButton))
            
            return Alert(title: Text(FlexText.LessonLockedTitle), message: Text(FlexText.LessonLockedMessage(cost: cost)), primaryButton: primaryButton, secondaryButton: secondaryButton)
        }
        
    }
    
}
