//
//  AnimatedCircle.swift
//  Saytoday
//
//  Created by Dylan Southard on 12/11/20.
//

import SwiftUI

struct AnimatedCircle: View {
    var reversed = false
    var color:Color
    var duration:Double = 1.5
    var delay:Double = 0
    @State var animationFinished:Bool = false
    @State var minScale:CGFloat = 0
    var animation:Animation {
        Animation.easeOut(duration: duration).repeatForever(autoreverses: false).delay(delay)
    }

    
    var body: some View {
        Circle()
            .fill(color)
            .scaleEffect(animationFinished ? CGFloat(0.9) : minScale)
            .opacity(animationFinished ? 0 : 0.8)
            .onAppear() {
                
                animationFinished = reversed
                withAnimation(self.animation, {
                    
                    self.animationFinished.toggle()
                })
            }
    }
}

struct AnimatedCircle_Previews: PreviewProvider {
    static var previews: some View {
        AnimatedCircle(color:.blue)
    }
}
