//
//  AnswerButton.swift
//  Saytoday
//
//  Created by Dylan Southard on 12/11/20.
//

import SwiftUI

struct SLButton: View {
    var diameter:CGFloat
    var type:SLButtonType
    @Binding var currentState:SLButtonState
    var onTap:()->Void
    @State var imageOpacity:Double = 1
    @State var tapActionInProgress = false
    
    
    var currentBackground:AnyView  {
        switch currentState {
        case .awaitingInput:
            return AnyView(Circle().foregroundColor(.speakBtnBkg))
        case .active:
            return AnyView(
                ZStack {
                    Circle().foregroundColor(.speakBtnBkg)
                    AnimatedCircle(color: .speakBtnAnimation)
                    AnimatedCircle(color: .speakBtnAnimation, delay:0.3)
                }
                )
        case .processing:
            return AnyView(
              
//                    Circle().foregroundColor(Color(UIColor.SpeakBtnBkg))
                ZStack {
                    AnimatedCircle(reversed: true, color: .speakBtnProcessingAnimation)
                    AnimatedCircle(reversed: true, color: .speakBtnProcessingAnimation, delay:0.3)
                }
                
                )
        
        }
    }
    
    var body: some View {
    
    
        
        ZStack {
            Circle()
                .strokeBorder(Color.speakBtnOutline, lineWidth: 2)
                .background(currentBackground)
                .frame(width: diameter, height: diameter, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
            
            
                Image(uiImage:UIImage.SLButton(for: type)!).resizable()
                    .renderingMode(.template)
                    .foregroundColor(.speakBtnImage)
                        
                        .aspectRatio(contentMode: .fit)
                        .scaledToFit()
                        .frame(width: diameter / 2, height: diameter / 2, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                    .opacity(tapActionInProgress ? imageOpacity : (self.currentState == .processing ? 0 : 1) )
                    
                
            
            
            

        }.onTapGesture(count: 1, perform: {
            self.tapActionInProgress = true
            onTap()
            print("current state: \(currentState) image opacity \(imageOpacity)")
            if (self.currentState == .processing) != (self.imageOpacity == 0) {
                withAnimation {
                    self.imageOpacity = self.currentState == .processing ? 0 : 1
                    
                }
                
                
            }
            self.tapActionInProgress = false
        })
        
    }
}

struct AnswerButton_Previews: PreviewProvider {
    static var previews: some View {
        SLButton(diameter: 300, type:.speak, currentState: .constant(.awaitingInput), onTap: {})
    }
}


enum SLButtonType {
    case speak
    case listen
}

enum SLButtonState:String, CaseIterable {
    case awaitingInput = "Speak Answer"
    case active = "Listen"
    case processing = "Processing"
}
