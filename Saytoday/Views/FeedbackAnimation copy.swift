//
//  FeedbackAnimation.swift
//  Saytoday
//
//  Created by Dylan Southard on 12/13/20.
//

import SwiftUI

struct FeedbackAnimation: View {
    var condition:ConditionalProperty
    @State var animationFinished:Bool = false
    var animation:Animation { Animation.easeOut(duration:0.5) }
    
    
    var body: some View {
        Image(uiImage:condition.image)
            .resizable()
            .renderingMode(.template)
            .foregroundColor(condition.color)
            .aspectRatio(contentMode: .fit)
            .scaleEffect(animationFinished ? CGFloat(1) : CGFloat(0.2))
            .opacity(animationFinished ? 0 : 1)
            .onAppear() {
                withAnimation(self.animation, {
                    self.animationFinished.toggle()
                })
            }
    }
}

struct FeedbackAnimation_Previews: PreviewProvider {
    static var previews: some View {
        FeedbackAnimation(condition:.correct)
    }
}

enum ConditionalProperty {
    case correct
    case incorrect
    
    var image:UIImage {
        switch self {
        case .correct:
            return UIImage.FeedbackCorrect
        case .incorrect:
            return UIImage.FeedbackIncorrect
        }
    }
    
    var color:Color {
        switch self {
        case .correct:
            return .cGreen
        case .incorrect:
            return .cRed
        }
    }
}
