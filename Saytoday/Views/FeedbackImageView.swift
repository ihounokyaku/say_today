////
////  FeedbackImageView.swift
////  MaximumEnglish
////
////  Created by Dylan Southard on 6/1/20.
////  Copyright © 2020 Dylan Southard. All rights reserved.
////
//
//import UIKit
//import SwiftUI
//
//
//
//struct FeedbackImage : UIViewRepresentable  {
//    var condition:ConditionalProperty
//    
//    func makeUIView(context: Context) -> FeedbackImageView {
//        return FeedbackImageView(condition)
//        }
//
//        func updateUIView(_ uiView: FeedbackImageView, context: Context) {
//            uiView.animate(condition: condition)
//        }
//    
//}
//
//
//class FeedbackImageView: UIView {
//    
//    
//    private var frameCenter:CGPoint {
//        return CGPoint(x: self.frame.width / 2, y: self.frame.height / 2)
//    }
//    
//    
//    //MARK: - =============== PROPERTIES ===============
//    var primaryDuration = 0.08
//    var secondaryDuration = 0.2
//    var tertiaryDuration = 0.1
//    var maxAlpha:CGFloat = 0.6
//    
//    
//    //MARK: - =============== INIT ===============
//    override init(frame: CGRect) {
//        super.init(frame: frame)
//        self.setUp()
//        
//    }
//    
//    init(_ condition:ConditionalProperty) {
//        self.init()
//        self.animate(condition: condition)
//    }
//     
//    required init?(coder: NSCoder) {
//        super.init(coder: coder)
//        self.setUp()
//    }
//    
//    func setUp() {
//        
//        self.isHidden = false
//        
//    }
//    
//    
//    
//    
//    func animate(condition:ConditionalProperty) {
//        let imageView = self.centerIconImageView(forCondition: condition)
//        let imageAnimation = self.centerIconImageView(forCondition: condition)
//        self.addSubview(imageView)
//        self.addSubview(imageAnimation)
//        self.isHidden = false
//       
//        UIView.animate(withDuration: self.primaryDuration, delay: 0, options: .curveEaseIn, animations: {
//            imageView.alpha = self.maxAlpha
//            imageAnimation.alpha = self.maxAlpha
//        }) { _ in
//            UIView.animate(withDuration: self.secondaryDuration, delay: 0, options: .curveEaseIn, animations: {
//                imageAnimation.frame.size = CGSize(width:imageAnimation.frame.width * 2, height:imageAnimation.frame.height * 2)
//                imageAnimation.center = self.frameCenter
//                imageAnimation.alpha = 0
//            }) { _ in
//                //On completion
//                imageAnimation.removeFromSuperview()
//                self.isHidden = self.secondaryDuration >= self.tertiaryDuration
//            }
//            
//            UIView.animate(withDuration: self.tertiaryDuration, delay: 0, options: .curveEaseIn, animations: {
//                imageView.alpha = 0
//            }) { _ in
//                //On completion
//                imageView.removeFromSuperview()
//                self.isHidden = self.secondaryDuration < self.tertiaryDuration
//            }
//        }
//    }
//    
//    private func centerIconImageView(forCondition condition:ConditionalProperty)-> UIImageView {
//        
//        let imageView = UIImageView(image:condition.image)
//        imageView.tintColor = condition.color
//        imageView.frame.size = CGSize(width: self.frame.width / 2, height: self.frame.height / 2)
////        let width = self.frame.width / 2
////        let height = self.frame.height / 2
////        imageView.frame = CGRect(x: (self.frame.width - width) / 2, y: (self.frame.height - height) / 2, width: width, height: height)
//        imageView.center = self.frameCenter
//        imageView.alpha = 0
//        return imageView
//    }
//    
//}
