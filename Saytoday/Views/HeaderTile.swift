//
//  SwiftUIView.swift
//  Saytoday
//
//  Created by Dylan Southard on 12/8/20.
//

import SwiftUI

struct SubTileData {
    var title:String
    var destination:()->AnyView
}

struct CollapsibleTile: View {
    var headerTitle:String
    var index:Int
    
    var subTiles:[SubTileData]
    @Binding var currentOpenHeader:Int
    @Binding var shouldAnimate:Bool
    var locked:Bool = false
    var cost:Int = 0
    var onTappedHeader:()->Void = {}
    var onTappedLocked:()->Void = {}
    var expanded:Bool {currentOpenHeader == index}
    
    
    var body: some View {
        VStack {
            HStack(alignment:.center) {
                Text(headerTitle)
                    .font(Font.TableRowTitle)
                Spacer()
                
                if locked {
                    VStack{
                        Image(uiImage: UIImage.Lock)
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(height:TableRowHeight / 4)
                        
                        if cost > 0 {
                            Text("\(cost)")
                                .font(Font.Cost)
                                .foregroundColor(.textDisabled)
                        }
                        
                    }
                } else {
                    Image(systemName:"chevron.down").rotationEffect(.degrees(expanded ? -180 : 0))
                        .animation( shouldAnimate ? .easeInOut : nil)
                }
                
            }.frame(height:TableRowHeight)
            .contentShape(Rectangle())
            .onTapGesture(count: /*@START_MENU_TOKEN@*/1/*@END_MENU_TOKEN@*/, perform: {
                shouldAnimate = true
                if !locked {
                    currentOpenHeader = currentOpenHeader == index ? -1 : index
                    onTappedHeader()
                } else {
                    onTappedLocked()
                }
                
                
            }).padding(.horizontal)
            .background(Color.tableBkg(index))
            
            
            VStack {
                ForEach(subTiles.indices, id:\.self) { index in
                    
                    NavigationLink(
                        destination: subTiles[index].destination()
                    ){
                        SubTile(title:subTiles[index].title, index: index).frame(height:expanded ? TableRowHeight:0)
                    }
                    .foregroundColor(.primary)
                    .padding(.horizontal)
                }
                
            }
            .opacity(expanded ? 1 : 0)
            
            
        
        }
        .animation(shouldAnimate ? .easeInOut : nil)
        
    }
    
}

struct HeaderTile_Previews: PreviewProvider {
    static var previews: some View {
        
        CollapsibleTile(headerTitle:"Course Title", index:0, subTiles: [SubTileData(title: "testing", destination: {AnyView(Text("woot"))})], currentOpenHeader: .constant(0), shouldAnimate: .constant(false), locked:true, cost:200)
        
    }
}
