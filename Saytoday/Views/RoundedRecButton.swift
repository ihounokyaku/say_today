//
//  RoundedRecButton.swift
//  Saytoday
//
//  Created by Dylan Southard on 12/11/20.
//

import SwiftUI

struct RoundedRecButton: View {
    var title:String
    var color:Color
    var accessory:UIImage?
    var height:CGFloat = 40
    var onTap:()-> Void
    
    
    var body: some View {
        
        Button(action:onTap) {
            
            ZStack {
                RoundedRectangle(cornerRadius: 5)
                    .fill(color)
                    .frame(width: height * 3, height: height, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                HStack {
                    Text(title)
                        .foregroundColor(.nextButtonTxt)
                        .font(Font.NextButton)
                    if let image = accessory {
                        Image(uiImage: image)
                            .resizable()
                            .renderingMode(.template)
                            .aspectRatio(contentMode: .fit)
                            .foregroundColor(.nextButtonTxt)
                            .frame(height:height / 2)
                        
                    }
                }
                
            }
            
                
        }
    }
}

struct RoundedRecButton_Previews: PreviewProvider {
    static var previews: some View {
        RoundedRecButton(title:"SHOW", color:.showButtonBkg, onTap:{})
    }
}
