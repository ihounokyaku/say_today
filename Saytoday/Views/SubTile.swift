//
//  SwiftUIView.swift
//  Saytoday
//
//  Created by Dylan Southard on 12/8/20.
//

import SwiftUI



struct SubTile: View {
    var title:String
    var index:Int

    
    var body: some View {
       
            HStack(alignment:.center) {
                Text(title)
                    .foregroundColor(.textPrimary)
                    .font(Font.TableRowTitle)
           Spacer()
                
            }.padding(.leading, 10)
        

    }
    
}

struct SubTile_Previews: PreviewProvider {
    static var previews: some View {
    
        SubTile(title:"blah", index:0)
        .previewLayout(.fixed(width: 300, height: 40))
        
        
    }
}
