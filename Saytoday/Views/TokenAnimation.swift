//
//  FeedbackAnimation.swift
//  Saytoday
//
//  Created by Dylan Southard on 12/13/20.
//

import SwiftUI

struct TokenAnimation: View {
    var amount:Int
    @EnvironmentObject var dm:DataManager
    @State var animationFinished:Bool = false
    var animation:Animation { Animation.easeOut(duration:1.5) }
    
    
    var body: some View {
        Text("+\(amount)")
            .font(Font.FeedbackToken)
            .scaleEffect(animationFinished ? CGFloat(1) : CGFloat(1))
           .opacity(animationFinished ? 0 : 1)
            .foregroundColor(.passText)
            .offset(CGSize(width:0, height: animationFinished ? -200 : 0))
            .onAppear() {
                withAnimation(self.animation, {
                    self.animationFinished.toggle()
                    dm.refreshTokens()
                })
            }
    }
}

struct TokenAnimation_Previews: PreviewProvider {
    static var previews: some View {
        TokenAnimation(amount:1)
    }
}
